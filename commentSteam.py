import praw
import sys
from lib import lyrics
from pprint import pprint

if __name__ == '__main__':
	paths = sys.argv[1:]
	# print lyrics.getnextFromFiles(paths, comment)
	r = praw.Reddit("lyric bot /u/autoTune_ v0.1")
	r.login()
	print "logged in!"
	for comment in praw.helpers.comment_stream(r, 'all', limit=None):
		try:
			if len(comment.body) > 100 or len(comment.body) <= 8:
				continue
			# print "processing: " + comment.body
			if comment.author.name == "autoTune_":
				continue
			#pprint(vars(comment)) 
			result = lyrics.getnextFromFiles(paths, comment.body)
			score = result[0]
			nextLyric = result[1]
			if score > 4:
				continue
			print ">replying to " + comment.body + "   >with " + nextLyric
			comment.reply(nextLyric)
		except:
			print "error"
