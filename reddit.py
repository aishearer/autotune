import praw
import sys
import time
from lib import lyrics
from pprint import pprint

if __name__ == '__main__':
    USERNAME = 'autoTune__'
    USER_AGENT = "automatic bard /u/%s v0.1" % USERNAME
    print "Using user agent: %s" % USER_AGENT
    r = praw.Reddit(user_agent=USER_AGENT)
    r.login(USERNAME)
    print "logged in as %s" % USERNAME
    subreddit = r.get_subreddit('all')
    while True:
        time.sleep(4)
        for submission in subreddit.get_hot(limit=50):
            print "processing a submission with %s comments" % len(submission.comments)
            for comment in praw.helpers.flatten_tree(submission.comments):
                try:
                    if comment.author.name == USERNAME:
                        continue
                    #result = lyrics.getnextFromFiles(paths, comment.body)
                    #comment.reply(nextLyric)
                    print comment.body
                except:
                    print "error"
